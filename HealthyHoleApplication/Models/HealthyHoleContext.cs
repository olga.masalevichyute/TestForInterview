﻿using Microsoft.EntityFrameworkCore;

namespace HealthyHoleApplication.Models
{
    public class HealthyHoleContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<Position> Positions { get; set; }

        public HealthyHoleContext(DbContextOptions<HealthyHoleContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
