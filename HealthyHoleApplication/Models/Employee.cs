﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HealthyHoleApplication.Models
{
    [Table("employees")]
    public class Employee
    {
        public Employee()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        [Column("id")]
        public Guid Id { get; private set; }

        [Required]
        [Column("surname")]
        public string Surname { get; set; }

        [Required]
        [Column("name")]
        public string Name { get; set; }

        [Column("middle_name")]
        public string MiddleName { get; set; }

        [Required]
        [Column("position_id")]
        public long PositionId { get; set; }

        [ForeignKey("PositionId")]
        public virtual Position Position { get; set; }
        public virtual ICollection<Shift> Shifts { get; set; }
    }
}
