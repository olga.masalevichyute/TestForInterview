﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HealthyHoleApplication.Models
{
    [Table("positions")]
    public class Position
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("id")]
        public long Id { get; set; }

        [Column("position_name")]
        public string PositionName { get; set; }

        [Column("start_shift")]
        public DateTime StartShift { get; set; }

        [Column("end_shift")]
        public DateTime EndShift { get; set; }

    }
}
