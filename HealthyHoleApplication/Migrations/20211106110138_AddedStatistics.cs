﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HealthyHoleApplication.Migrations
{
    public partial class AddedStatistics : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "is_satisfactory",
                table: "shifts",
                type: "INTEGER",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "end_shift",
                table: "positions",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "start_shift",
                table: "positions",
                type: "TEXT",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "is_satisfactory",
                table: "shifts");

            migrationBuilder.DropColumn(
                name: "end_shift",
                table: "positions");

            migrationBuilder.DropColumn(
                name: "start_shift",
                table: "positions");
        }
    }
}
