﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HealthyHoleApplication.Migrations
{
    public partial class MoreMigrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_shifts_positions_PositionId",
                table: "shifts");

            migrationBuilder.DropIndex(
                name: "IX_shifts_PositionId",
                table: "shifts");

            migrationBuilder.DropColumn(
                name: "PositionId",
                table: "shifts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "PositionId",
                table: "shifts",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_shifts_PositionId",
                table: "shifts",
                column: "PositionId");

            migrationBuilder.AddForeignKey(
                name: "FK_shifts_positions_PositionId",
                table: "shifts",
                column: "PositionId",
                principalTable: "positions",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
