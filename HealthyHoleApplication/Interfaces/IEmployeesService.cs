﻿using HealthyHoleApplication.Models;
using HealthyHoleApplication.ViewModels;
using System;
using System.Collections.Generic;

namespace HealthyHoleApplication.Interfaces
{
    public interface IEmployeesService
    {
        Employee CreateEmployee(EmployeeViewModel newModelValues);
        Employee DeleteEmployee(Guid employeeId);
        Employee GetEmployeeById(Guid employeeId);
        List<Employee> GetEmployees(long? positionId);
        Employee UpdateEmployee(Guid employeeId, EmployeeViewModel newModelValues);
    }
}