﻿using HealthyHoleApplication.Models;
using HealthyHoleApplication.ViewModels;
using System.Collections.Generic;

namespace HealthyHoleApplication.Interfaces
{
    public interface IShiftsService
    {
        Shift CreateShiftEnd(CheckpointViewModel checkpointCrossing);
        Shift CreateShiftStart(CheckpointViewModel checkpointCrossing);
        List<ViolationStatistics> GetUnsatisfactoryEmployee(ViolationStatisticsFilter filteringValues);
    }
}