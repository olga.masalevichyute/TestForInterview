﻿using HealthyHoleApplication.Models;
using System.Collections.Generic;

namespace HealthyHoleApplication.Interfaces
{
    public interface IPositionsService
    {
        List<Position> GetAllPositions();
    }
}