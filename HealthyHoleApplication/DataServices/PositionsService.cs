﻿using HealthyHoleApplication.Interfaces;
using HealthyHoleApplication.Models;
using HealthyHoleApplication.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthyHoleApplication.DataServices
{
    public class PositionsService : IPositionsService
    {
        private readonly HealthyHoleContext _context;

        public PositionsService(HealthyHoleContext context)
        {
            _context = context;
        }

        public List<Position> GetAllPositions() =>
            _context.Positions.ToList();
    }
}
