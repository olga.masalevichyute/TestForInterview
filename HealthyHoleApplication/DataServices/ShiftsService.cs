﻿using HealthyHoleApplication.Interfaces;
using HealthyHoleApplication.Models;
using HealthyHoleApplication.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthyHoleApplication.DataServices
{
    public class ShiftsService : IShiftsService
    {
        private readonly HealthyHoleContext _context;
        private readonly IEmployeesService _employeesService;

        public ShiftsService(HealthyHoleContext context, IEmployeesService employeesService)
        {
            _context = context;
            _employeesService = employeesService;
        }

        private List<Shift> GetUnsatisfactoryShifts(Guid employeeId, int month, int year)
        {
            var shifts = _context.Shifts.Where(s => s.EmployeeId.Equals(employeeId))
                .Where(s => s.EndTime.Year == year && s.EndTime.Month == month)?
                .Where(s => !s.IsSatisfactory)?
                .ToList();

            return shifts;
        }

        public List<ViolationStatistics> GetUnsatisfactoryEmployee(ViolationStatisticsFilter filteringValues)
        {
            var shifts = GetUnsatisfactoryShifts(filteringValues.EmployeeId, filteringValues.Month, filteringValues.Year);

            var unsatisfactory = shifts.Join(_context.Employees,
                s => s.EmployeeId,
                e => e.Id,
                (s, e) => new ViolationStatistics
                {
                    Surname = e.Surname,
                    Name = e.Name,
                    MiddleName = e.MiddleName,
                    StartShift = s.StartTime,
                    EndShift = s.EndTime,
                    HoursWorked = s.HoursWorked
                })
                .ToList();

            return unsatisfactory;
        }

        private List<Shift> GetArrivalRecords(Guid employeeId)
        {
            var shifts = _context.Shifts
                .Where(s => s.EmployeeId.Equals(employeeId))?
                .Where(s => s.StartTime != default && s.EndTime == default)
                .ToList();

            return shifts;
        }

        public Shift CreateShiftStart(CheckpointViewModel checkpointCrossing)
        {
            var shifts = GetArrivalRecords(checkpointCrossing.EmployeeId);
            if (shifts.Count != 0)
                throw new Exception("Нет данных о завершении смены сотрудника с Id = '" + checkpointCrossing.EmployeeId + ".");

            Shift shift = new()
            {
                StartTime = checkpointCrossing.CheckpointCrossingTime,
                EmployeeId = checkpointCrossing.EmployeeId
            };

            _context.Shifts.Add(shift);
            _context.SaveChanges();

            shift = GetShiftById(shift.Id);

            return shift;
        }

        public Shift CreateShiftEnd(CheckpointViewModel checkpointCrossing)
        {
            var shift = _context.Shifts
                .Where(s => s.EmployeeId.Equals(checkpointCrossing.EmployeeId))
                .FirstOrDefault(s => s.StartTime != default && s.EndTime == default);

            if (shift == null)
                throw new Exception("Нет данных о начале смены сотрудника с Id = '" + checkpointCrossing.EmployeeId + ".");

            shift.EndTime = checkpointCrossing.CheckpointCrossingTime;
            shift.HoursWorked = shift.EndTime - shift.StartTime;

            if (shift.HoursWorked.TotalMilliseconds <= 0)
                throw new Exception("Дата окончания смены должна быть позже даты начала.");

            Employee employee = _employeesService.GetEmployeeById(checkpointCrossing.EmployeeId);
            shift.IsSatisfactory = (shift.StartTime.Hour <= employee.Position.StartShift.Hour &&
                shift.EndTime.Hour >= employee.Position.EndShift.Hour);

            _context.SaveChanges();

            shift = GetShiftById(shift.Id);

            return shift;
        }

        private Shift GetShiftById(long id)
        {
            var shift = _context.Shifts.Include(s => s.Employee).FirstOrDefault(p => p.Id == id);

            if (shift == default)
                throw new Exception(string.Format("Смена c ID = '{0}' не найдена в БД.", id));

            return shift;
        }
    }
}
