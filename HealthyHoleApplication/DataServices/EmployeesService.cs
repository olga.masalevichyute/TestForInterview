﻿using HealthyHoleApplication.Interfaces;
using HealthyHoleApplication.Models;
using HealthyHoleApplication.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HealthyHoleApplication.DataServices
{
    public class EmployeesService : IEmployeesService
    {
        private readonly HealthyHoleContext _context;

        public EmployeesService(HealthyHoleContext context)
        {
            _context = context;
        }

        public List<Employee> GetEmployees(long? positionId)
        {
            return positionId == null ?
                _context.Employees.Include(e => e.Position).Include(e => e.Shifts).ToList() :
                _context.Employees.Where(e => e.PositionId == positionId).Include(e => e.Position).Include(e => e.Shifts).ToList();
        }

        public Employee GetEmployeeById(Guid employeeId)
        {
            var employee = _context.Employees.Include(e => e.Position).Include(e => e.Shifts).FirstOrDefault(e => e.Id == employeeId);

            if (employee == default)
                throw new Exception(string.Format("Cотрудник c ID = '{0}' не найден в БД.", employeeId));

            return employee;
        }

        public Employee CreateEmployee(EmployeeViewModel newModelValues)
        {
            if (!newModelValues.Validate())
                throw new Exception("Не заполнены имя, фамилия или должность сотрудника.");

            var position = GetPositionById(newModelValues.PositionId);
            Employee employee = new()
            {
                Surname = newModelValues.Surname,
                Name = newModelValues.Name,
                MiddleName = newModelValues.MiddleName,
                PositionId = position.Id
            };

            _context.Employees.Add(employee);
            _context.SaveChanges();

            employee = GetEmployeeById(employee.Id);

            return employee;
        }

        public Employee UpdateEmployee(Guid employeeId, EmployeeViewModel newModelValues)
        {
            if (employeeId == default)
                throw new Exception("Не указано Id сотрудника");

            var employee = _context.Employees.FirstOrDefault(e => e.Id.Equals(employeeId));

            if (employee == default)
                throw new Exception(string.Format("Cотрудник c ID = '{0}' не найден в БД.", employeeId));

            employee.Surname = string.IsNullOrEmpty(newModelValues.Surname) ? employee.Surname : newModelValues.Surname;
            employee.Name = string.IsNullOrEmpty(newModelValues.Name) ? employee.Name : newModelValues.Name;
            employee.MiddleName = string.IsNullOrEmpty(newModelValues.MiddleName) ? employee.MiddleName : newModelValues.MiddleName;
            employee.PositionId = newModelValues.PositionId ?? employee.PositionId;
            _context.SaveChanges();

            employee = GetEmployeeById(employeeId);

            return employee;
        }

        public Employee DeleteEmployee(Guid employeeId)
        {
            var employee = _context.Employees.FirstOrDefault(e => e.Id.Equals(employeeId));
            if (employee == default)
                throw new Exception(string.Format("Cотрудник c ID = '{0}' не найден в БД.", employeeId));

            _context.Employees.Remove(employee);
            _context.SaveChanges();
            return employee;
        }

        private Position GetPositionById(long? id)
        {
            var position = _context.Positions.FirstOrDefault(p => p.Id == id);

            if (position == default)
                throw new Exception(string.Format("Должность c ID = '{0}' не найден в БД.", id));

            return position;
        }

    }
}
