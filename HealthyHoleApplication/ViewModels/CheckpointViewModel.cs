﻿using System;

namespace HealthyHoleApplication.ViewModels
{
    public class CheckpointViewModel
    {
        public Guid EmployeeId { get; set; }
        public DateTime CheckpointCrossingTime { get; set; }
    }
}
