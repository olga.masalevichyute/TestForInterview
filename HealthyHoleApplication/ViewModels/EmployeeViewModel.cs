﻿using System.Linq;

namespace HealthyHoleApplication.ViewModels
{
    public class EmployeeViewModel
    {
        public string Surname { get; set; }
        
        public string Name { get; set; }

        public string MiddleName { get; set; }

        public long? PositionId { get; set; }

        public bool Validate()
        {
            var propertyInfo = GetType()
               .GetProperties()
               .Where(pi => !pi.Name.Equals("MiddleName"))
               .FirstOrDefault(pi => pi.GetValue(this) == null);

            return propertyInfo == default;
        }
    }
}
