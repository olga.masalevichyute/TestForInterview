﻿using System;

namespace HealthyHoleApplication.ViewModels
{
    public class ViolationStatisticsFilter
    {
        public Guid EmployeeId { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
    }
}
