﻿using System;

namespace HealthyHoleApplication.ViewModels
{
    public class ViolationStatistics
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string MiddleName { get; set; }
        public DateTime StartShift { get; set; }
        public DateTime EndShift { get; set; }
        public TimeSpan HoursWorked { get; set; }
    }
}
