﻿using HealthyHoleApplication.Interfaces;
using HealthyHoleApplication.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;

namespace HealthyHoleApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckpointController : ControllerBase
    {
        private readonly IShiftsService _shiftsService;

        public CheckpointController(IShiftsService shiftsService)
        {
            _shiftsService = shiftsService;
        }

        // POST api/Checkpoint/startshift
        [HttpPost("startshift")]
        public IActionResult StartShift([FromBody] CheckpointViewModel checkpointCrossing)
        {
            try
            {
                var shift = _shiftsService.CreateShiftStart(checkpointCrossing);
                return Ok(new { shift });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    message = "При попытке начать смену произошла ошибка: " + ex.Message
                });
            }
        }

        // POST api/Checkpoint/endshift
        [HttpPost("endshift")]
        public IActionResult EndShift([FromBody] CheckpointViewModel checkpointCrossing)
        {
            try
            {
                var shift = _shiftsService.CreateShiftEnd(checkpointCrossing);
                return Ok(new { shift });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    message = "При попытке завершить смену произошла ошибка: " + ex.Message
                });
            }
        }
    }
}
