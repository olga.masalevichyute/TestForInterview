﻿using HealthyHoleApplication.Interfaces;
using HealthyHoleApplication.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;

namespace HealthyHoleApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HRDepartmentController : ControllerBase
    {
        private readonly IEmployeesService _employeesService;
        private readonly IPositionsService _positionsService;
        private readonly IShiftsService _shiftsService;

        public HRDepartmentController(
            IEmployeesService employeesService, 
            IPositionsService positionsService, 
            IShiftsService shiftsService)
        {
            _employeesService = employeesService;
            _positionsService = positionsService;
            _shiftsService = shiftsService;
        }

        // GET: api/HRDepartment/allpositions
        [HttpGet("allpositions")]
        public IActionResult GetAllPositions()
        {
            try
            {
                var positions = _positionsService.GetAllPositions();
                return Ok(new { positions });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    message = "При получении списка должностей произошла ошибка: " + ex.Message
                });
            }
        }

        // GET api/HRDepartment/employees?positionId=1
        [HttpGet("employees")]
        public IActionResult GetEmployees([FromQuery] long? positionId)
        {
            try
            {
                var employees = _employeesService.GetEmployees(positionId);
                return Ok(new { employees });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    message = "При получении списка сотрудников произошла ошибка: " + ex.Message
                });
            }
        }

        // GET api/HRDepartment/employees/C7338B5C-A768-4C2A-841D-75F2B5C99553
        [HttpGet("employees/{employeeId}")]
        public IActionResult GetEmployeeById(Guid employeeId)
        {
            try
            {
                var employee = _employeesService.GetEmployeeById(employeeId);
                return Ok(new { employee });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    message = "При получении списка сотрудников произошла ошибка: " + ex.Message
                });
            }
        }

        // POST api/HRDepartment/employee
        [HttpPost("employee")]
        public IActionResult CreateEmployee([FromBody] EmployeeViewModel employeeFromBody)
        {
            try
            {
                var employee = _employeesService.CreateEmployee(employeeFromBody);
                return Ok(new { employee });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    message = "При создании сотрудника произошла ошибка: " + ex.Message
                });
            }
        }

        // PUT api/HRDepartment/updateemployee/C7338B5C-A768-4C2A-841D-75F2B5C99553
        [HttpPut("employee/{employeeId}")]
        public IActionResult UpdateEmployee(Guid employeeId, [FromBody] EmployeeViewModel employeeFromBody)
        {
            try
            {
                var employee = _employeesService.UpdateEmployee(employeeId, employeeFromBody);
                return Ok(new { employee });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    message = "При обновлении данных о сотруднике произошла ошибка: " + ex.Message
                });
            }
        }

        // DELETE api/HRDepartment/employee/4619BDD4-AC2C-4E78-95AF-8E86632A7D5F
        [HttpDelete("employee/{employeeId}")]
        public IActionResult DeleteEmployee(Guid employeeId)
        {
            try
            {
                var employee = _employeesService.DeleteEmployee(employeeId);
                return Ok(new { employee });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    message = "При удалении сотрудника произошла ошибка: " + ex.Message
                });
            }
        }

        // GET api/HRDepartment/violationstatistics?EmployeeId=4619BDD4-AC2C-4E78-95AF-8E86632A7D5F&Month=10&Year=2021
        [HttpGet("violationstatistics")]
        public IActionResult GetViolationStatistics([FromQuery] ViolationStatisticsFilter filteringValues)
        {
            try
            {
                var employee = _shiftsService.GetUnsatisfactoryEmployee(filteringValues);

                if (employee.Count == 0)
                    return Ok(new
                    {
                        message = string.Format("У сотрудника с Id = '{0}' нет нарушений за {1}.{2}.",
                        filteringValues.EmployeeId, filteringValues.Month, filteringValues.Year)
                    });

                return Ok(new { employee });
            }
            catch (Exception ex)
            {
                return BadRequest(new
                {
                    message = "При получении статистики нарушений произошла ошибка: " + ex.Message
                });
            }
        }
    }
}
